﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EndController : MonoBehaviour {

	public GameObject player1, player2, groundBG,milkywayBG;
	public Text title1, title2;

	// Use this for initialization
	void Start () {
		AudioManager.Main.PlayNewSound ("game_music");
		if (PlayerController.isGameOver == 1) {
			//player 1 won
			title1.text = "You won!";
			title2.text = "You lost!";
			title1.color = new Color(39/255f,174/255f,96/255f);
			title2.color = new Color(231/255f,76/255f,60/255f);


			player1.GetComponent<Animator>().SetTrigger("Happy");
			player2.GetComponent<Animator>().SetTrigger("Sad");



		} else {
			//player 2 won
			title2.text = "You won!";
			title1.text = "You lost!";
			title2.color = new Color(39/255f,174/255f,96/255f);
			title1.color = new Color(231/255f,76/255f,60/255f);

			player2.GetComponent<Animator>().SetTrigger("Happy");
			player1.GetComponent<Animator>().SetTrigger("Sad");

			Vector3 temp = milkywayBG.transform.position;
			milkywayBG.transform.position = groundBG.transform.position;
			groundBG.transform.position = temp;
		}

		LeanTween.rotateX (title1.gameObject, 25f, 0.5f).setEase (LeanTweenType.easeSpring).setLoopPingPong(-1);
		LeanTween.rotateX (title2.gameObject, 25f, 0.5f).setEase (LeanTweenType.easeSpring).setLoopPingPong(-1);

		LeanTween.scale (title1.gameObject, new Vector3(1.3f,1.3f,0), 0.5f).setEase (LeanTweenType.easeSpring).setLoopPingPong(-1);
		LeanTween.scale (title2.gameObject, new Vector3(1.3f,1.3f,0), 0.5f).setEase (LeanTweenType.easeSpring).setLoopPingPong(-1);

		//Invoke ("startGame", 5f);
	}

	void startGame(){
		PlayerController.playerReadyCount = 0;
		PlayerController.isGameOver = 0;
		Application.LoadLevel("Game");
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp (KeyCode.R)) {
			startGame();
		}
	}
}
