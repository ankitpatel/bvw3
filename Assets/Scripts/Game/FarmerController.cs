﻿using UnityEngine;
using System.Collections;

public class FarmerController : MonoBehaviour {

	string[] farmerCollisionSounds = new string[]{"FarmerCollision1","FarmerCollision2","FarmerCollision3"};
	bool isCollisionSoundPlaying = false;
	Animator farmerAnim;
	Vector3 farmerTargetPos;
	// Use this for initialization
	void Start () {
		farmerAnim = GetComponent<Animator> ();
		farmerTargetPos = transform.position;

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter(Collision collision) {


		switch (collision.gameObject.tag) {
		
		case "Tree":
		case "Cloud":
		case "Meteor":
		case "Boundry":
			playCowCollisionSound();
			break;
		default:
			
			break;
		}
		
	}

	void playCowCollisionSound(){
		if (isCollisionSoundPlaying == false) {
			AudioManager.Main.PlayNewSound (farmerCollisionSounds[Random.Range(0,farmerCollisionSounds.Length)]);

			isCollisionSoundPlaying = true;
			LeanTween.delayedCall(2f,()=>{
				isCollisionSoundPlaying = false;
			});
		}
	}
}
