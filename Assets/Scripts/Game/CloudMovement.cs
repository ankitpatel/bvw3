﻿using UnityEngine;
using System.Collections;

public class CloudMovement : MonoBehaviour {

	public float time;
	// Use this for initialization
	void Start () {
		time = Mathf.Clamp (time, 4, 10);
		startAnimation();

	}

	void startAnimation(){
		if (gameObject.transform.position.x < 20) {
			LeanTween.moveX (gameObject, -1f, time).setDelay (Random.Range (0, 15f)).setLoopPingPong (-1);

		} else {
			LeanTween.moveX (gameObject, 0.3f, time).setDelay (Random.Range (0, 15f)).setLoopPingPong (-1);

		}

	}
	// Update is called once per frame
	void Update () {
	
	}
}
