﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	public KeyCode leftTrigger,rightTrigger;
	public float speed;
	public int playerIndex;
	public float upForce,downForce; 
	public Text infoText,counterText,playerText;
	public static int isGameOver = 0; //0
	public Transform startPoint,endPoint;
	public GameObject farmer,playerDot;
	public static int playerReadyCount = 0;
	public RawImage title;

	Rigidbody rigidbody;
	enum Direction{Left,Right,None,Straight};
	Direction prevDirection,curDirection;
	bool isInputEnabled = true;
	ParticleSystem ps;
	bool isGameStarted = false;
	string[] squeezeSounds = new string[]{"squeeze2","squeeze3"};
	string[] boingSounds = new string[]{"boing11","boing88"};
	string[] cowHappySounds = new string[]{"CowHappy"};
	string[] cowCollisionSounds = new string[]{"CowCollision"};
	string[] cowFallingSounds = new string[]{"Cow_falling_reverb"};
	string[] farmerHappySounds = new string[]{"FarmerHappy1","FarmerHappy2"};
	int successfulTriggers = 0;
	int didTriggerStart = 0;
	int successTriggerCount = 20;
	Animator cowAnim;
	bool didFirstPull = false;
	string lastTrigger;
	bool isCollisionSoundPlaying = false;
	bool isFallingSoundPLaying = false;
	int cowFallingStarted = 1000;
	int cowFallingMaxLimit = 1000;
	Direction lastInput;

	// Use this for initialization
	void Start () {

		cowAnim = GetComponent<Animator>();
		infoText.text = "Pull the udders to fly";
		counterText.text = "";
		ps = GetComponentInChildren<ParticleSystem> ();
		stopMilkParticles();
		rigidbody = GetComponent<Rigidbody> ();

		LeanTween.rotateX (title.gameObject, 15f, 0.5f).setEase (LeanTweenType.easeSpring).setLoopPingPong(-1);
		LeanTween.rotateX (infoText.gameObject, 30f, 0.5f).setEase (LeanTweenType.easeSpring).setLoopPingPong(-1);



	}

	void restart(){
		playerReadyCount = 0;
		isGameOver = 0;
		Application.LoadLevel("Game");
	}
	
	// Update is called once per frame
	void Update () {

		if (upForce < 600f) {
			upForce += Time.deltaTime*50f;
			//print("upForce "+ upForce);
		}

		if (Input.GetKeyUp (KeyCode.R)) {
			restart();
		}


		if (isInputEnabled == false)
			return;

		prevDirection = curDirection;
		if (Input.GetKeyDown (KeyCode.Space) || Input.GetKeyDown (leftTrigger) == true && Input.GetKeyDown (rightTrigger) == true) {
			//Debug.Log ("Trigger Straight");
			curDirection = Direction.Straight;
			addForce (curDirection);
			isInputEnabled = false;
			Invoke ("enableInput", 0.3f);
			cowFallingStarted = cowFallingMaxLimit; 
		}
		else if (Input.GetKeyUp (leftTrigger) == true) {
			//turn left
			//Debug.Log ("Trigger Left");
			onLeftTrigger();

		} else if (Input.GetKeyUp (rightTrigger) == true) {
			//turn right
			//Debug.Log ("Trigger Right");
			onRightTrigger();
		} else {
			//Debug.Log ("Trigger None");
			curDirection = Direction.None;

			if(isGameStarted)
			{
				//Debug.Log("cowFallingStarted"+ cowFallingStarted);
				cowFallingStarted--;
				if(cowFallingStarted == 0 && transform.position.y > startPoint.position.y + 5f)
				{
					cowFallingStarted = cowFallingMaxLimit; 
					AudioManager.Main.PlayNewSound (cowFallingSounds[Random.Range(0,cowFallingSounds.Length)]);

				}


				if(lastTrigger != "Normal")
				{
					cowAnim.SetTrigger("Normal");
					lastTrigger = "Normal";
				}


			}
		} 


		if(playerReadyCount == 2 && isGameStarted == false)
		{
			isGameStarted = true;
			Invoke ("startGame", 1f);
		}

		updateProgress ();

	
	}

	void onRightTrigger(){
		curDirection = Direction.Right;
		lastInput = Direction.Right;

		addForce (curDirection);
		
		isInputEnabled = false;
		Invoke ("enableInput", 0.3f);
		cowFallingStarted = cowFallingMaxLimit; 
	}

	void onLeftTrigger(){
		curDirection = Direction.Left;
		lastInput = Direction.Left;
		addForce (curDirection);
		
		isInputEnabled = false;
		Invoke ("enableInput", 0.3f);
		cowFallingStarted = cowFallingMaxLimit; 
	}

	void updateProgress(){
		float totalDis = endPoint.position.y - startPoint.position.y;
		float curDis = transform.position.y - startPoint.position.y; //Mathf.Lerp (transform.position.y - startPoint.position.y, 0f, totalDis);
		float progress = (float) (curDis / (float)totalDis);
		progress = Mathf.Clamp (progress, 0.0f, 1.0f);
		//Debug.Log ("progress:" + playerIndex + ", perc :" + progress);


		Vector3 curDotPos = playerDot.transform.position; 
		curDotPos.y = Screen.height*progress;
		playerDot.transform.position = curDotPos;
	}


	void addForce(Direction direction){

		if (rigidbody.velocity == Vector3.zero) {
			rigidbody.velocity = new Vector3( -Mathf.Lerp(0,upForce,Time.deltaTime),Mathf.Lerp(0,upForce*2f,Time.deltaTime),0f);
		}

		if (direction == Direction.Left) {

			cowAnim.SetTrigger("Left");
			lastTrigger = "Left";
			rigidbody.velocity = new Vector3( -Mathf.Lerp(0,upForce,Time.deltaTime),Mathf.Lerp(0,upForce*2f,Time.deltaTime),0f);
			startMilkParticles();


		} else if (direction == Direction.Right) {

			cowAnim.SetTrigger("Right");
			lastTrigger = "Right";
			rigidbody.velocity = new Vector3(Mathf.Lerp(0,upForce,Time.deltaTime),Mathf.Lerp(0,upForce*2f,Time.deltaTime),0f);
			startMilkParticles();

		} else if (direction == Direction.Straight) {
			rigidbody.velocity = new Vector3(0,Mathf.Lerp(0,upForce*6f,Time.deltaTime),0f);
			startMilkParticles();

		} else {
			rigidbody.AddForce (0f, -downForce, 0);
		}
	

	}

	void enableInput(){
		isInputEnabled = true;
	}

	void startMilkParticles(){

		infoText.enabled = false;
		title.enabled = false;
		playerText.enabled = false;

		didTriggerStart = 1;
		LeanTween.delayedCall(0.2f,() => {

			if(didTriggerStart == 1)
			{
				successfulTriggers++;
				//Debug.Log("successfulTriggers :"+ successfulTriggers);
				if(successfulTriggers == successTriggerCount)
				{
					successfulTriggers = 0;
					AudioManager.Main.PlayNewSound (farmerHappySounds[Random.Range(0,farmerHappySounds.Length)]);
				}
			}

			didTriggerStart = 0;
		});

		if (ps != null) {

			ps.Play();
			ps.enableEmission = true;
			AudioManager.Main.PlayNewSound (squeezeSounds[Random.Range(0,squeezeSounds.Length)]);

		}
	}

	void stopMilkParticles(){
		if (ps != null) {
			ps.Stop();
			ps.enableEmission = false;
		}
	}


	void resetForce(){
		rigidbody.velocity = Vector3.zero;
		rigidbody.angularVelocity = Vector3.zero;
	}

	void startGame(){
		AudioManager.Main.PlayNewSound("321Go");

		counterText.text = "3";
		LeanTween.delayedCall (gameObject, 2f, () => {
			counterText.text = "2";
			LeanTween.delayedCall (gameObject, 2f, () => {
				counterText.text = "1";
				LeanTween.delayedCall (gameObject, 1f, () => {
					counterText.text = "Go!";
					enablePlayer();
					LeanTween.delayedCall (gameObject, 0.6f, () => {
						counterText.text = "";
					});
				});
			});
		});



	}

	void enablePlayer(){
		rigidbody.isKinematic = false;
		isGameStarted = true;
	}

	void disablePlayer(){
		rigidbody.isKinematic = true;
	}

	void OnCollisionEnter(Collision collision) {


		switch (collision.gameObject.tag) {
		case "StartLine":
			cowAnim.SetTrigger("Normal");
			lastTrigger = "Normal";
			//disable startline
			collision.gameObject.SetActive (false);
			rigidbody.isKinematic = true;

			AudioManager.Main.PlayNewSound("CowSwoosh");

			LeanTween.move (gameObject, startPoint.position, 0.5f).setOnComplete (
				() => {
				counterText.text = "Ready!";
				//check for both player
				playerReadyCount++;


				}
			);

		
			break;
		case "FinishLine":

			collision.gameObject.SetActive (false);
			//rigidbody.isKinematic = true;
			AudioManager.Main.PlayNewSound ("Win");
			AudioManager.Main.PlayNewSound ("CowHappy");
			LeanTween.delayedCall(0.35f,()=>{

				AudioManager.Main.PlayNewSound ("FarmerHappy1");

			});

			addForce(Direction.Straight);

			isGameOver = playerIndex;
			Invoke("startEndScene",1f);

			break;
		case "Tree":

			upForce = Mathf.Clamp(upForce - 50,400,650);

			cowAnim.SetTrigger("Sad");
			lastTrigger = "Sad";
			didTriggerStart = 0;
			successfulTriggers = 0;

			playCowCollisionSound();

			break;
		case "Cloud":
		case "Blimp":

			upForce = Mathf.Clamp(upForce - 50,400,650);

			cowAnim.SetTrigger("Sad");
			lastTrigger = "Sad";

			didTriggerStart = 0;
			successfulTriggers = 0;

			playCowCollisionSound();

			break;
		case "Meteor":
			upForce = Mathf.Clamp(upForce - 50,400,650);

			cowAnim.SetTrigger("Sad");
			lastTrigger = "Sad";

			didTriggerStart = 0;
			successfulTriggers = 0;

			//add meteor force
			Vector3 forceVec = collision.gameObject.transform.forward * 5f;
			rigidbody.AddForce (forceVec.x, forceVec.y, forceVec.z);

			playCowCollisionSound();

			break;
		case "Boundry":
			cowAnim.SetTrigger("Sad");
			lastTrigger = "Sad";
			didTriggerStart = 0;
			successfulTriggers = 0;

			/*
			if(lastInput == Direction.Left){
				//print("right trigger");
				onRightTrigger();

			}else if(lastInput == Direction.Right){
				//print("left trigger");
				onLeftTrigger();
			}*/

			AudioManager.Main.PlayNewSound (boingSounds[Random.Range(0,boingSounds.Length)]);

			break;
		default:

			break;
		}

	}

	void playCowCollisionSound(){
		if (isCollisionSoundPlaying == false) {
			AudioManager.Main.PlayNewSound (cowCollisionSounds[Random.Range(0,cowCollisionSounds.Length)]);
			AudioManager.Main.PlayNewSound (boingSounds[Random.Range(0,boingSounds.Length)]);

			isCollisionSoundPlaying = true;
			LeanTween.delayedCall(2f,()=>{
				isCollisionSoundPlaying = false;
			});
		}
	}

	void startEndScene(){
		Application.LoadLevel("End");

	}
}
