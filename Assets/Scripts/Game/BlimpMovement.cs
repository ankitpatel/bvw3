﻿using UnityEngine;
using System.Collections;

public class BlimpMovement : MonoBehaviour {

	public int direction = 1;
	// Use this for initialization
	void Start () {
		if (direction == 1) {
			LeanTween.moveLocalX (gameObject,35f,6f).setDelay(Random.Range(0,6f)).setRepeat(-1);
			
		} else if(direction == -1){
			LeanTween.moveLocalX (gameObject,-30f,6f).setDelay(Random.Range(0,6f)).setRepeat(-1);
			
		}
	}
	
	// Update is called once per frame
	void Update () {

	}


}
