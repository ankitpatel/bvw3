﻿using UnityEngine;
using System.Collections;

public class Meteor : MonoBehaviour {

	// Use this for initialization
	void Start () {
		startAnimation();
	}

	void startAnimation(){

		LeanTween.moveLocalX (gameObject,30f,3f).setDelay(Random.Range(0,5f)).setLoopPingPong (-1);
		LeanTween.rotateY(gameObject,10f,0.3f).setRepeat(-1);

	}

	// Update is called once per frame
	void Update () {
	
	}

	 
}
