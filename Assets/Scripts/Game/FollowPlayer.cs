﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour {

	public GameObject player;
	float offset; 
	Vector3 oriPos;
	// Use this for initialization
	void Start () {
		offset = transform.position.y - player.transform.position.y;
		oriPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {

		//transform.position.y + offset
		Vector3 newPos = new Vector3 (oriPos.x, player.transform.position.y + offset, oriPos.z);
		transform.position = newPos;

	}
}
