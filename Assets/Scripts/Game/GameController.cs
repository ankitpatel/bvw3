﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{
	public AudioClip gameMusic;
	AudioSource audioSource;

	// Use this for initialization
	void Start ()
	{
		audioSource = GetComponent<AudioSource> ();
		audioSource.clip = gameMusic;
		audioSource.Play ();
		//AudioManager.Main.PlayNewSound ("game_music", true, true);

	}



	ArrayList getChildObjectsWithTag (GameObject parent, string tag)
	{
		ArrayList list = new ArrayList ();
		
		Transform[] allChildren = parent.GetComponentsInChildren<Transform> ();
		foreach (Transform child in allChildren) {
			// do whatever with child transform here
			if (child.tag == tag) {
				list.Add (child.gameObject);
			}
		}
		
		return list;
	}




	// Update is called once per frame
	void Update ()
	{
	
	}
}
